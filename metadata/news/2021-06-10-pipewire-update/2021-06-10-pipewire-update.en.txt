Title: PipeWire 0.3.30 update
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2021-06-10
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: media/pipewire[<0.3.30]

With PipeWire 0.3.30 we follow upstream which now installs the PipeWire
configuration into /usr/share/pipewire instead of /etc/pipewire, while
/etc/pipewire will be the place for local overrides.
To not run into problems due to changes in the configuration files and to
benefit from further upstream improvements please make sure only your
local overrides reside in /etc/pipewire. If you haven't made any manual
changes to these files it's strongly advised to remove them.
