# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="git://oscaf.git.sourceforge.net/gitroot/oscaf/${PN}"
    require scm-git
fi

require sourceforge [ project=oscaf ] cmake

export_exlib_phases src_configure

SUMMARY="Common OSCAF desktop ontologies"
DESCRIPTION="
The vision of the Social Semantic Desktop defines a user's personal information
environment as a source and end-point of the Semantic Web: Knowledge workers
comprehensively express their information and data with respect to their own
conceptualizations. Semantic Web languages and protocols are used to formalize
these conceptualizations and for coordinating local and global information
access. The Resource Description Framework [RDF] serves as a common data
representation format. We identified several additional requirements for
high-level knowledge representation on the social semantic desktop. With a
particular focus on addressing certain limitations of RDF, we engineered a
novel representational language akin to RDF and the Web Ontology Language
[OWL], plus a number of other high-level ontologies. Together, they provide a
means to build the semantic bridges necessary for data exchange and application
integration on distributed social semantic desktops. Although initially
designed to fulfill requirements for the [NEPOMUK] project, these ontologies
are useful for the semantic web community in general.
"
HOMEPAGE+="
    http://www.semanticdesktop.org/ontologies
"

LICENCES="|| ( BSD-3 CCPL-Attribution-3.0 )"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
"

# This package's contents are independent of architecture, so it shouldn't
# install to /usr/${host} as cmake.exlib wants it to
shared-desktop-ontologies_src_configure() {
    export PREFIX=/usr
    cmake_src_configure
}

