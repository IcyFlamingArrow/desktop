# Copyright 2012 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qmake [ slot=5 ]

SUMMARY="Free cross-platform LaTeX editor"
DESCRIPTION="
Texmaker is a free, modern and cross-platform LaTeX editor for linux, macosx and
windows systems that integrates many tools needed to develop documents with LaTeX,
in just one application.

Texmaker includes unicode support, spell checking, auto-completion, code folding
and a built-in pdf viewer with synctex support and continuous view mode.
"
HOMEPAGE="http://www.xm1math.net/texmaker/"
DOWNLOADS="http://www.xm1math.net/${PN}/${PNV}.tar.bz2"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

QT_MIN_VER=5.7.0

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}]
    recommendation:
        app-text/texlive-core [[ description = [ Needed to produce dvi/pdf from
                                                 (La)TeX sources ] ]]
    suggestion:
        app-text/ghostscript [[ description = [ For the LaTeX to html
                                                conversion command ] ]]
"
# NOTE: Bundles pdfium and hunspell

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-install-destinations.patch
)

EQMAKE_PARAMS=(
    PREFIX="/usr"
    BINDIR="/usr/$(exhost --target)/bin"
    # Apparently a basic included browser, would need QtWebEngine
    INTERNALBROWSER="no"
)

