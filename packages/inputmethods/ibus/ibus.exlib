# Copyright 2009-2013 Hong Hao <oahong@oahong.me>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'ibus-1.1.0.20090211.ebuild' from Gentoo, which is:
#    Copyright 1999-2009 Gentoo Foundation

require github [ release=${PV} suffix=tar.gz ] \
    gtk-icon-cache gsettings \
    python [ blacklist="2" multibuild=false ] \
    vala [ with_opt=true vala_dep=true ] \
    option-renames [ renames=[ 'gtk3 gtk' ] ]

export_exlib_phases src_install pkg_postinst pkg_postrm

SUMMARY="Intelligent Input Bus"
DESCRIPTION="
IBus is an Intelligent Input Bus. It is a new input framework for Linux OS.
It provides full featured and user friendly input method user interface.
It also may help developers to develop input method easily.
"

UPSTREAM_DOCUMENTATION="https://github.com/ibus/ibus/wiki [[ lang = en,zh_CN ]]"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="gobject-introspection gtk gtk-doc wayland X
    gtk? (
        ( providers: gtk2 [[ description = [ Build GTK+ 2 immodule ] ]]
                     gtk3 [[ description = [ Build GTK+ 3 immodule ] ]]
                     gtk4 [[ description = [ Build GTK 4 immodule ] ]]
        ) [[ number-selected = at-least-one ]]
    )
    vapi [[ requires = gobject-introspection ]]
    ( linguas: ar as bg bn bn_IN ca cs da de en_GB es et eu fa fi fr gu he hi hu ia id it ja kn ko
               lv ml mn mr nb nl or pa pl pt_BR ru sq sr sr@latin sv ta te tg tr uk ur vi zh_CN
               zh_HK zh_TW
    )
"

# require X and a running ibus-daemon
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-doc/gtk-doc[>=1.32-r1]
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
        x11-apps/xkeyboard-config
        wayland? ( sys-libs/wayland-protocols )
    build+run:
        app-text/cldr-emoji-annotation
        app-text/iso-codes
        app-text/unicode-data
        app-text/unicode-emoji
        core/json-glib
        dev-libs/glib:2[>=2.46.0]
        dev-libs/libxml2:2.0
        gnome-bindings/pygobject:3[>=3.0.0][python_abis:*(-)?]
        gnome-desktop/dconf[>=0.13.4]
        sys-apps/dbus
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        providers:gtk2? ( x11-libs/gtk+:2 )
        providers:gtk3? ( x11-libs/gtk+:3[>=3.12.0][wayland?][X?] )
        providers:gtk4? ( x11-libs/gtk:4.0[wayland?][X?] )
        x11-libs/pango
        wayland? (
            sys-libs/wayland[>=1.2.0]
            x11-libs/libxkbcommon
        )
        X? (
            x11-libs/gtk+:3 [[ note = [ for XIM ] ]]
            x11-libs/libX11
            x11-libs/libXfixes
            x11-libs/libXi
        )
    recommendation:
        gnome-desktop/librsvg:2
    suggestion:
        inputmethods/ibus-qt
        inputmethods/ibus-pinyin
        inputmethods/ibus-table
        inputmethods/ibus-chewing
        inputmethods/ibus-anthy
        inputmethods/ibus-hangul
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=/usr
    --exec_prefix=/usr/$(exhost --target)
    --includedir=/usr/$(exhost --target)/include
    --disable-appindicator
    --disable-install-tests
    --disable-memconf
    --disable-python2
    --disable-python-library
    --disable-tests
    --enable-dconf
    --enable-emoji-dict
    --enable-engine
    --enable-setup
    --enable-surrounding-text
    --with-unicode-emoji-dir=/usr/share/unicode/emoji
    --with-ucd-dir=/usr/share/unicode-data/
    --with-python=${PYTHON}
    --with-panel-icon-keyboard=yes
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'vapi vala'
    'wayland'
    'X ui'
    'X xim'
    'providers:gtk2'
    'providers:gtk3'
    'providers:gtk4'
)

update_gtk_immodules() {
    option providers:gtk2 && gtk-query-immodules-2.0 --update-cache
    option providers:gtk3 && gtk-query-immodules-3.0 --update-cache
}

ibus_src_install() {
    default
    edo rmdir "${IMAGE}"/usr/share/${PN}/engine
}

ibus_pkg_postinst() {
    update_gtk_immodules
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

ibus_pkg_postrm() {
    update_gtk_immodules
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

