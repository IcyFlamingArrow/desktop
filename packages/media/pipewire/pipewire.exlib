# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" suffix=tar.bz2 new_download_scheme=true ] \
    meson [ meson_minimum_version=0.59.0 ] systemd-service udev-rules

export_exlib_phases src_install

SUMMARY="PipeWire is a project that aims to greatly improve handling of audio and video under Linux"

HOMEPAGE+=" https://pipewire.org/"

LICENCES="
    GPL-2 [[ note = [ pipewire-jack/jack/control.h ] ]]
    LGPL-2.1 [[ note = [ spa/plugins/alsa ] ]]
    MIT
"
SLOT="0"

MYOPTIONS="
    alsa [[ description = [ Build Pipewire's ALSA plugin and provide a default output ] ]]
    avahi
    bluetooth [[ description = [ Build Pipewire's BlueZ plugin ] ]]
    camera [[ description = [ Support camera devices using libcamera ] ]]
    dbus
    doc
    echo-cancel-webrtc [[ description = [ Build Pipewire's WebRTC-based echo canceller module ] ]]
    ffmpeg [[ description = [ Build Pipewire's FFmpeg plugin ] ]]
    gstreamer [[ description = [ Build Pipewire's GStreamer plugins ] ]]
    jack [[ description = [ Build Pipewire's JACK plugin ] ]]
    pulseaudio [[ description = [ Module to tunnel audio to/from a (remote) PulseAudio server ] ]]
    systemd
    (
        aptx [[ description = [ Support the Bluetooth aptX codec ] ]]
        fdk-aac [[ description = [ Support the Bluetooth AAC codec ] ]]
        ldac [[ description = [ Support the Bluetooth LDAC codec ] ]]
    ) [[ *requires = [ bluetooth ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-python/docutils
        sys-devel/gettext
        sys-libs/vulkan-headers[>=1.1.69]
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/glib:2[>=2.32.0]
        media-libs/libsndfile[>=1.0.20]
        sys-libs/ncurses
        sys-libs/readline:=
        sys-libs/vulkan-loader[>=1.1.69]
        sys-sound/alsa-lib[>=1.1.7]
        aptx? ( media-libs/libfreeaptx )
        avahi? ( net-dns/avahi[dbus] )
        bluetooth? (
            dev-libs/libusb:1
            media-libs/sbc
            net-wireless/bluez
        )
        camera? (
            media-libs/libcamera
            x11-dri/libdrm[>=2.4.98]
        )
        dbus? ( sys-apps/dbus )
        echo-cancel-webrtc? ( media-libs/webrtc-audio-processing[>=0.2&<1.0] )
        fdk-aac? ( media-libs/fdk-aac )
        ffmpeg? ( media/ffmpeg )
        gstreamer? (
            media-libs/gstreamer:1.0[>=1.10.0]
            media-plugins/gst-plugins-base:1.0
        )
        jack? ( media-sound/jack-audio-connection-kit[>=1.9.10] )
        ldac? ( media-libs/libldac )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        pulseaudio? ( media-sound/pulseaudio )
        systemd? ( sys-apps/systemd )
    post:
        media/wireplumber
    recommendation:
        sys-apps/rtkit [[
            description = [ Enable real-time capability required by libpipewire-module-rtkit ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dalsa=enabled
    -Daudioconvert=enabled
    -Daudiomixer=enabled
    -Daudiotestsrc=enabled
    -Dcontrol=enabled
    -Ddocdir=/usr/share/doc/${PNVR}
    -Devl=disabled
    -Dexamples=disabled
    -Dinstalled_tests=disabled
    -Djack-devel=false
    -Dlv2=disabled
    -Dman=enabled
    -Dpipewire-v4l2=enabled
    -Dpw-cat=enabled
    -Draop=disabled
    -Droc=disabled
    -Dsdl2=disabled
    -Dsession-managers=/usr/$(exhost --target)/bin/wireplumber
    -Dsndfile=enabled
    -Dspa-plugins=enabled
    -Dsupport=enabled
    -Dsystemd-user-unit-dir=${SYSTEMDUSERUNITDIR}
    -Dtest=enabled
    -Dudev=enabled
    -Dudevrulesdir=${UDEVRULESDIR}
    -Dv4l2=enabled
    -Dvideoconvert=enabled
    -Dvideotestsrc=enabled
    -Dvolume=enabled
    -Dvulkan=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'alsa pipewire-alsa'
    'aptx bluez5-codec-aptx'
    avahi
    'bluetooth bluez5'
    'bluetooth bluez5-backend-hfp-native'
    'bluetooth bluez5-backend-hsphfpd'
    'bluetooth bluez5-backend-hsp-native'
    'bluetooth bluez5-backend-ofono'
    'bluetooth libusb'
    'camera libcamera'
    dbus
    'doc docs'
    echo-cancel-webrtc
    'fdk-aac bluez5-codec-aac'
    ffmpeg
    gstreamer
    'gstreamer gstreamer-device-provider'
    jack
    'jack pipewire-jack'
    'ldac bluez5-codec-ldac'
    'pulseaudio libpulse'
    systemd
    'systemd systemd-system-service'
    'systemd systemd-user-service'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

pipewire_src_install() {
    meson_src_install

    if option alsa ; then
        # see ALSA plugin in INSTALL.md
        dodir /etc/alsa/conf.d
        edo cp "${IMAGE}"/usr/share/alsa/alsa.conf.d/50-pipewire.conf "${IMAGE}"/etc/alsa/conf.d/50-pipewire.conf
        edo cp "${IMAGE}"/usr/share/alsa/alsa.conf.d/99-pipewire-default.conf "${IMAGE}"/etc/alsa/conf.d/99-pipewire-default.conf
    fi
}

