# Copyright 2018-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=Vulkan-Tools tag=v${PV} ] \
    cmake

SUMMARY="Vulkan Utilities and Tools"
DESCRIPTION="
Vulkan tools and utilities that can assist development by enabling developers to verify their
applications correct use of the Vulkan API.
* VulkanInfo
* Vkcube and Vkcube++ demo applications
"
HOMEPAGE+=" https://www.khronos.org/vulkan"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    cube [[ description = [ Build the Vkcube and Vkcube++ demo applications ] ]]
    ( X wayland ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        sys-libs/vulkan-headers[>=$(ever range 1-3)]
        virtual/pkg-config
        cube? ( dev-lang/glslang )
        wayland? ( sys-libs/wayland-protocols )
    build+run:
        sys-libs/vulkan-loader[>=$(ever range 1-3)]
        X? (
            x11-libs/libxcb
            x11-libs/libX11
        )
        wayland? ( sys-libs/wayland )
"

src_configure() {
    local cmakeparams=(
        -DBUILD_CUBE:BOOL=$(option cube TRUE FALSE)
        -DBUILD_ICD:BOOL=FALSE
        -DBUILD_VULKANINFO:BOOL=TRUE
        -DBUILD_WERROR:BOOL=FALSE
        -DBUILD_WSI_DIRECTFB_SUPPORT:BOOL=FALSE
        -DBUILD_WSI_XCB_SUPPORT:BOOL=$(option X TRUE FALSE)
        -DBUILD_WSI_XLIB_SUPPORT:BOOL=$(option X TRUE FALSE)
        -DBUILD_WSI_WAYLAND_SUPPORT:BOOL=$(option wayland TRUE FALSE)
        -DGLSLANG_INSTALL_DIR:PATH=/usr/$(exhost --target)
        -DVulkanRegistry_DIR:PATH=/usr/share/vulkan/registry
    )

    # vkcube can only be built with support for one WSI target, default
    # to XCB if both X and wayland are enabled.
    if option X ; then
        cmakeparams+=( -DCUBE_WSI_SELECTION:STRING=XCB )
    fi

    if ! option X && option wayland ; then
        cmakeparams+=( -DCUBE_WSI_SELECTION:STRING=WAYLAND )
    fi

    ecmake "${cmakeparams[@]}"
}

