# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Command line tools that assist applications"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"
DOWNLOADS="https://portland.freedesktop.org/download/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# Tests need a running X server
RESTRICT="test"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.1.2
        app-text/xmlto
    suggestion:
        (
            net-www/links:*
            net-www/lynx
            net-www/w3m
        ) [[
            *description = [ Fallback web browser for URL opening ]
        ]]
        x11-apps/xset [[ description = [ xdg-screensaver can set DPMS. ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-open-for-post-1.1.3-development.patch
    "${FILES}"/0002-xdg-open-better-pcmanfm-check-BR106636-BR106161.patch
    "${FILES}"/0003-xdg-email-Support-for-Deepin.patch
    "${FILES}"/0004-Restore-matching-of-older-deepin-names.patch
    "${FILES}"/0005-xdg-open-handle-file-localhost.patch
    "${FILES}"/0006-test-lib.sh-run-eat-xdg-open-s-exit-code.patch
    "${FILES}"/0007-Fix-a-bug-when-xdg-terminal-needs-gsettings-to-get-t.patch
    "${FILES}"/0008-Fixes-x-argument-which-is-the-default-for-gnome-mate.patch
    "${FILES}"/0009-xdg-screensaver-Sanitise-window-name-before-sending-.patch
    "${FILES}"/0010-xdg-su-fix-some-easy-TODOs.patch
    "${FILES}"/0011-xdg-open-fix-comment-typo.patch
    "${FILES}"/0012-Enable-cinnamon-screensaver-for-xdg-aware-desktop-en.patch
    "${FILES}"/0013-support-digits-in-uri-scheme-regex.patch
    "${FILES}"/0014-xdg-mime-return-correct-exit-code-for-GNOME.patch
    "${FILES}"/0015-fixed-166-xdg-open-dose-not-search-correctly-in-dire.patch
    "${FILES}"/0016-Fix-xdg-settings-support-for-default-web-browser-for.patch
)

